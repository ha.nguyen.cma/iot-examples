import time
import serial
import threading
from flask import Flask, render_template
app = Flask(__name__)

import queue

cmdQueue = queue.Queue()

@app.route('/update/<int:led>/<int:state>')
def update(led,state):
    global data
    if (led<3 and state<2):
        data[1][led] = state
        color_str = 'rgb'
        state_str ='01'
        cmd = color_str[led] + state_str[state]
        cmdQueue.put(cmd)

    return 'data=' + str(data)

@app.route('/auto')
def auto():
    return 'data=' + str(data)


@app.route('/')
def hello_world():
    #return render_template('view.html', data=data)
    return 'data=' + str(data)

def update_data(s):
    global data
    try:
        #data = float(s)
        return True
    except ValueError:
        return False

def updateThread():
    print("hello from thread")
    global finish
    with serial.Serial('/dev/ttyUSB0', 9600, timeout=1) as ser:
        while not finish:
            if not cmdQueue.empty():
                cmd = cmdQueue.get()
                ser.write(cmd.encode(encoding='ascii'))



if __name__=="__main__":
    global finish
    finish = False

    global data
    data=[[0,0,0],[0,0,0]]


    rthread = threading.Thread(target=updateThread)
    rthread.start()

    app.run(host="0.0.0.0")

    print("exiting")

    finish = True
    while rthread.is_alive():
        time.sleep(1)


