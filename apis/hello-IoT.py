import time
import serial
import threading
from flask import Flask, render_template
app = Flask(__name__)


@app.route('/auto')
def auto():
    return 'data=' + str(data)

@app.route('/')
def hello_world():
    return render_template('view.html', data=data)


def update_data(s):
    global data
    try:
        data = float(s)
        return True
    except ValueError:
        return False

def readingThread():
    print("hello from thread")
    global is_reading
    with serial.Serial('/dev/ttyUSB1', 9600, timeout=1) as ser:
        while is_reading:
            line = ser.readline()
            print(line)
            update_data(line.strip())


if __name__=="__main__":
    global is_reading
    is_reading = True

    global data
    data=0


    rthread = threading.Thread(target=readingThread)
    rthread.start()

    app.run(host="0.0.0.0")

    print("exiting")

    is_reading = False
    while rthread.is_alive():
        time.sleep(1)


