#include <Arduino_JSON.h>


int pin_r = 10;
int pin_g = 11;
int pin_b = 12;

JSONVar led;
bool invalidate;
char color, state;

void setup() {
  Serial.begin(9600);
  while (!Serial) continue;
  
  pinMode(pin_r, OUTPUT);
  pinMode(pin_g, OUTPUT);
  pinMode(pin_b, OUTPUT);

  digitalWrite(pin_r, LOW);
  digitalWrite(pin_g, LOW);
  digitalWrite(pin_b, LOW);

  //led["s1_r"] = "0";
  //led["s1_g"] = "0";
  //led["s1_b"] = "0";
  
  led["s2_r"] = "0";
  led["s2_g"] = "0";
  led["s2_b"] = "0";

  color = 'r';
  state = '0';
  
  invalidate = true;
}




void loop() {
  if (invalidate) {
    Serial.println("...");
    invalidate = false;
    String key = "s2_" + String(color);
    Serial.println(key);
    led[key] = String(state);
    //Serial.println(led);
    
    if (led["s2_r"]==String("1")) digitalWrite(pin_r, HIGH);
    else digitalWrite(pin_r, LOW);
    
    if (led["s2_g"]==String("1")) digitalWrite(pin_g, HIGH);
    else digitalWrite(pin_g, LOW);

    if (led["s2_b"]==String("1")) digitalWrite(pin_b, HIGH);
    else digitalWrite(pin_b, LOW);
  }
 
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    color = (char)Serial.read();
    if (color=='r' || color=='g' || color=='b') {
      // wait for data
      while (!Serial.available()) continue;
      // read data
      state = (char)Serial.read();
      if (state=='0' || state =='1') {
        invalidate=true;
      }
    }
  }
}
